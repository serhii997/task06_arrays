package com.movchan.Queue;

import com.movchan.Logical.ViewArrays;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.PriorityQueue;

public class ViewPriorityQueue {
    private final static Logger LOGGER = LogManager.getLogger(ViewPriorityQueue.class);
    private PriorityQueue<Person> personPriorityQueue = new PriorityQueue<>();

    public ViewPriorityQueue() {
        personPriorityQueue.add(new Person("Sergio",22));
        personPriorityQueue.add(new Person("Petro",7));
        personPriorityQueue.add(new Person("Vlad",21));
    }

    public void runPriorityQueue(){
        while (!personPriorityQueue.isEmpty()){
            LOGGER.info(personPriorityQueue.poll());
        }
    }

}
