package com.movchan.Logical;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ViewArrays {

    private int[] firstMass = new int[]{1, 2, 3, 2, 4, 6, 5, 5, 2};
    private int[] secondMass = new int[]{5, 2, 9, 5, 5, 10, 4, 12};
    private ArraysFunction arraysFunction = new ArraysFunction();

    public void runArray(){
//        for (Integer integer: arraysFunction.addAnotherElement(firstMass,secondMass)) {
//            LOGGER.info(integer);
//        }
//        for (Integer integer: arraysFunction.addSameElement(firstMass,secondMass)) {
//            LOGGER.info(integer);
//        }
        new ArraysFunction().removeSameElement(firstMass);
    }
}
