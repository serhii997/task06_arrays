package com.movchan.Logical;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ArraysFunction {
    private final static Logger LOGGER = LogManager.getLogger(ViewArrays.class);

    public int[] addSameElement(int[] firstMass, int[] secondMass){
        int[] thirdMass = new int[greaterLength(firstMass,secondMass)];
        for (int i = 0; i < firstMass.length; i++) {
            int temp = firstMass[i];
            for (int j = 0; j < secondMass.length; j++) {
                if(temp == secondMass[j]){
                    thirdMass[j] = secondMass[j];
                    break;
                }
            }
        }

        outPut(thirdMass);
        return null;
    }

    public int[] addAnotherElement(int[] firstMass, int[]secondMass){
        int[] thirdMass = firstMass;
        int count = 0;
        for (int i = 0; i < firstMass.length; i++) {
            for (int j = 0; j < secondMass.length; j++) {
                if(firstMass[i] == secondMass[j]){
                    thirdMass = removeElement(thirdMass, firstMass[i]);
                    count++;
                }
            }
        }
        int[] result = new int [thirdMass.length - count];
        for (int i = 0; i < result.length; i++) {
            result[i] = thirdMass[i];
        }
        outPut(thirdMass);
        return null;
    }

    private int[] removeElement(int[] mass, int element){
        int[] result = new int[mass.length];
        for (int i = 0; i < mass.length; i++) {
            if(mass[i] != element){
                result[i] = mass[i];
            }
        }
        return result;
    }

    public int[] removeSameElement(int[] arr){
        boolean[] mask = new boolean[arr.length];
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            int temp = arr[i];
            for (int j = i+1; j < arr.length; j++) {
                if(temp == arr[j]){
                    mask[j] = true;

                }
            }
        }
        for (int i = 0; i < arr.length ; i++) {
            if(mask[i]){
                count++;
            }
        }

        return overwrite(arr,mask,count);
    }

    private int[] overwrite(int[] arr, boolean[] mask, int length){

        int[] newMass = new int[arr.length-length];
        for (int i = 0, j = 0; i < arr.length; i++) {
            if(!mask[i]){
                newMass[j++] = arr[i];
            }

        }
        outPut(newMass);
        return newMass;
    }

    private void outPut(int[] arr){
        for (int i = 0; i < arr.length; i++) {
            LOGGER.info(arr[i]);
        }
    }

    private int greaterLength(int[] firstMass, int[] secondMass) {
        return firstMass.length > secondMass.length ? firstMass.length : secondMass.length;
    }

}
