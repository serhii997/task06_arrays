package com.movchan.Container;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Objects;

public class MyContainer<T> {
    private final static Logger LOGGER = LogManager.getLogger(MyContainer.class);
    private T t;
    private List<? extends Integer> integers;

    public MyContainer() {
    }

    public MyContainer(T t) {
        this.t = t;
    }

    public T get() {
        return t;
    }

    public void set(T t) {
        this.t = t;
    }

    public List<? extends Integer> getIntegers() {
        return integers;
    }

    public void setIntegers(List<? extends Integer> integers) {
        this.integers = integers;
    }

    public void output(){
        integers.forEach( n -> LOGGER.info(n));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MyContainer<?> that = (MyContainer<?>) o;
        return Objects.equals(t, that.t) &&
                Objects.equals(integers, that.integers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(t, integers);
    }

    @Override
    public String toString() {
        return "MyContainer{" +
                "t=" + t +
                ", integers=" + integers +
                '}';
    }
}
