package com.movchan.Container;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;

public class ViewContainer {
    private final static Logger LOGGER = LogManager.getLogger(ViewContainer.class);

    public void runContainer(){
        MyContainer<String> container = new MyContainer<>();
        container.set("bla bla bla");
        String s = container.get();
        LOGGER.info(s);
        container.setIntegers(new ArrayList<Integer>(Arrays.asList(10,45,23,654)));
        container.output();
    }
}
