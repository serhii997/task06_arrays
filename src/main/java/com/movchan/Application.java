package com.movchan;

import com.movchan.Container.ViewContainer;
import com.movchan.Logical.ViewArrays;
import com.movchan.Queue.ViewPriorityQueue;
import com.movchan.game.view.ViewGame;

public class Application {
    public static void main(String[] args) {
//        new ViewArrays().runArray();
//        new ViewPriorityQueue().runPriorityQueue();
//        new ViewContainer().runContainer();
        new ViewGame().start();
    }
}
