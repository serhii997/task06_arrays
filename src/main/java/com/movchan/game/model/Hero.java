package com.movchan.game.model;

import java.util.Random;

public class Hero implements ModelHero{
    private static int power = 25;

    public Hero() {
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        power = power;
    }

    @Override
    public String toString() {
        return "Hero{" +
                "power=" + power +
                '}';
    }

    @Override
    public int addPower(int score) {
        power += score;
        return power;
    }

    @Override
    public int subtractionPower(int score) {
        power -= score;
        return power;
    }

}
