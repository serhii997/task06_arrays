package com.movchan.game.model;

public interface ModelHero {
    int addPower(int score);
    int subtractionPower(int score);
}
