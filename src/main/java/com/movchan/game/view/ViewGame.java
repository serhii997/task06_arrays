package com.movchan.game.view;

import com.movchan.game.control.Controller;

public class ViewGame {
    public void start(){
        new Controller().startGame();
    }
}
