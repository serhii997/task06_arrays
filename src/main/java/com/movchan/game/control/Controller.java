package com.movchan.game.control;

import com.movchan.game.model.Hero;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Controller implements ControllerInterface {
    public final static Logger LOGGER = LogManager.getLogger(ControllerInterface.class);
    private Hero hero;
    private final int countDoor = 10;
    private List<Boolean> doors;
    private List<Integer> points;

    public Controller() {
        hero = new Hero();
        doors = new ArrayList<>();
        points = new ArrayList<>();
    }

    @Override
    public void startGame() {
        Scanner scanner = new Scanner(System.in);
        creatDoor();
        outputDoor();
        output();
        while (true) {
            System.out.println("Please choose door");
            chooseDoor(scanner.nextInt());
            if(hero.getPower() <= 0){
                break;
            }
        }
    }

    private void output() {
        System.out.println(hero.toString());
    }

    private void creatDoor(){
        for (int i = 0; i < countDoor; i++) {
            doors.add(new Random().nextBoolean());
            if(doors.get(i)){
                points.add(new Random().nextInt(70)+10);
            }else{
                points.add(new Random().nextInt(95)+5);
            }
        }
    }
    private void outputDoor(){
        for (int i = 0; i < countDoor; i++) {
            if (doors.get(i)){
                LOGGER.info("Artefact is this door = " + (i+1) + " score = " +points.get(i));
            }else{
                LOGGER.info("Monster is this door = " + (i+1) + " score = " +points.get(i));
            }
        }
    }

    private void chooseDoor(int numberDoor){
        if(doors.get(numberDoor)){

            hero.addPower(points.get(numberDoor));
        }else {
            hero.subtractionPower(points.get(numberDoor));
        }
        output();
    }

}
